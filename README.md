# IMU Metric Plotter

This is a python based program for real-time plotting metrics from an IMU such as Acceleration and Gyroscope metrics.

## Requirements

* [python 3.10+](https://www.python.org/downloads/release/python-3100/)
* [pipenv](https://pipenv.pypa.io/en/latest/)
* [tail](https://man7.org/linux/man-pages/man1/tail.1.html)

## Installation

Run the following command to setup the python environment.

```bash
pipenv install
```

## Preperation

This program expects a file named imu.dat for data to be streamed from.  Each line in the file should have the following format:

```bash
time,acceleration_x,accleration_y,acceleration_z,gyroscope_x,gyroscope_y,gyroscope_z
```

Where time is in nanoseconds, acceleration and gyroscope can be in any units just ensure the values are numerical.

This program uses the tail command to read the last entry in the imu.dat file.  The expectation is that another program will be populating
the imu.dat file such as OpenHMD or Mondado.

Here is an example command that pipes the output of the monado cli where the program uses the U_LOG_RAW function to write to the console:

```bash
monado-cli test >& imu.dat
```

## Running the program

The program can be run with the following flag

```bash
Usage -a -g
 Where -a is for displaying accelleration
 -g is for displaying gyroscope
 ```

### Acceleration

Running the following command will create a graph plotting the acceleration only.

```bash
./plot.bash -a
```

### Gyroscope

Running the following command will create a graph for plotting the gyroscope only.

```bash
./plot.bash -g
```

### Acceleration and Gyroscope

Two instances of this program can be launched on seperate consoles to display both acceleration and gyroscopes in different windows.
Just run the above commands in a separate terminal.

## Tips

* Bitcoin: [bc1qvuf05a6fvsezn5449snnnqud2ul3eu0n50jue2](bitcoin:BC1QVUF05A6FVSEZN5449SNNNQUD2UL3EU0N50JUE2?label=MonkyGames%20Donation)
* Ethereum: [0xDE5520EDa276F910206A98d2b9A70431CeC8AB5F](https://etherscan.io/address/0xDE5520EDa276F910206A98d2b9A70431CeC8AB5F)
