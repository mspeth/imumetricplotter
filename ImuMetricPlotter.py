import numpy as np
import time
import matplotlib
matplotlib.use('GTK4Agg')
from matplotlib import pyplot as plt
import subprocess
import matplotlib.animation as animation
from datetime import datetime
import sys

if len(sys.argv) == 1:
    print("Usage -a -g\n","Where -a is for displaying accelleration\n","-g is for displaying gyroscope")
    quit()

if(sys.argv[1] == '-a'):
    is_accel = True
    is_gyro  = False
elif(sys.argv[1] == '-g'):
    is_accel = False
    is_gyro  = True
else:
    print("Usage -a -g\n","Where -a is for displaying accelleration\n","-g is for displaying gyroscope")
    quit()

imu_filename = 'imu.dat'
def read_values():
    f = subprocess.Popen(['tail','-n','1',imu_filename],\
            stdout=subprocess.PIPE,stderr=subprocess.PIPE)

    line = f.stdout.readline().decode('UTF-8')
    #print(line)

    line_array = line.split(',')
    #seconds = float(line_array[0].strip()) // 1000000000
    miliseconds = float(line_array[0].strip()) // 1000000
    #dt = datetime.fromtimestamp(seconds)
    #time = dt.strftime('%M:%S')

    ax = float(line_array[1].strip())
    ay = float(line_array[2].strip())
    az = float(line_array[3].strip())

    gx = float(line_array[4].strip())
    gy = float(line_array[5].strip())
    gz = float(line_array[6].strip())
    return (miliseconds,ax,ay,az,gx,gy,gz)

#print(read_values())

# Initialize Plot
if is_accel:
    fig = plt.figure(num='Nolo Raw Acceleration Readings')

else:
    fig = plt.figure(num='Nolo Raw Gyroscope Readings')
#fig = plt.figure(num="Nolo Raw Acceleration Readings")
subplot = fig.add_subplot(1,1,1)
ts = []
axs = []
ays = []
azs = []
gxs = []
gys = []
gzs = []

size = -1000


def animate(i, ts, axs, ays, azs,gxs,gys,gzs):

    # Read temperature (Celsius) from TMP102
    tup = read_values()
    #print(tup)

    # Add x and y to lists
    ts.append(tup[0])
    axs.append(tup[1])
    ays.append(tup[2])
    azs.append(tup[3])
    gxs.append(tup[4])
    gys.append(tup[5])
    gzs.append(tup[6])

    # Limit x and y lists to 20 items
    ts = ts[size:]
    axs = axs[size:]
    ays = ays[size:]
    azs = azs[size:]
    gxs = gxs[size:]
    gys = gys[size:]
    gzs = gzs[size:]

    if is_accel:
        # Draw x and y lists
        subplot.clear()
        subplot.plot(ts,axs, label="x")
        subplot.plot(ts,ays, label="y")
        subplot.plot(ts,azs, label="z")
        subplot.legend()
        # Format plot
        plt.xticks(rotation=45, ha='right')
        plt.subplots_adjust(bottom=0.15,left=0.25)
        plt.title('Nolo Raw Acceleration Reads')
        #plt.ylabel('m/s^2 ?')
        plt.ylabel('RAW')
        plt.xlabel("ms")
    else:
        subplot.clear()
        subplot.plot(ts,gxs, label="x")
        subplot.plot(ts,gys, label="y")
        subplot.plot(ts,gzs, label="z")
        subplot.legend()
        # Format plot
        plt.xticks(rotation=45, ha='right')
        plt.subplots_adjust(bottom=0.15,left=0.25)
        plt.title('Nolo Raw Gyroscope Reads')
        #plt.ylabel('rad/s ?')
        plt.ylabel('RAW')
        plt.xlabel("ms")

    #plt.ylim(10000,18000)


# Set up plot to call animate() function periodically
ani = animation.FuncAnimation(fig, animate, fargs=(ts,axs, ays, azs, gxs,gys,gzs), interval=100)
plt.show()
